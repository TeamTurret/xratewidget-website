<?php
	include __DIR__.'/../page-hits.php';
	pageHits("XRateWidget", $_SERVER["REMOTE_ADDR"]);
?>

<html>

	<head>
		<title>XRateWidget - Live Exchange Rate & Clock Widget</title>

		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">

		<script type="text/javascript" src="js/jquery2.0.0.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function()
			{
				$("#contact-us-btn").on("click", function()
				{
					var submit = true;

					$("#input-name").css("border-bottom-color","#38c89c");
					$("#input-msg").css("border-bottom-color","#38c89c");
					$("#input-email").css("border-bottom-color","#38c89c");

					if ( $("#input-name").val()=="" )
					{
						$("#input-name").css("border-bottom-color","#ff9999");
						submit = false;
					}
					if ( $("#input-msg").val()=="" )
					{
						$("#input-msg").css("border-bottom-color","#ff9999");
						submit = false;
					}
					if ( $("#input-email").val()=="" )
					{
						$("#input-email").css("border-bottom-color","#ff9999");
						submit = false;
					}
					else if ( isEmailValid($("#input-email").val())==false )
					{
						$("#input-email").css("border-bottom-color","#ff9999");
						submit = false;
					}
					if ( submit == true )
					{
						$("#contact-us-btn").val("Submitting Data. Please wait...");
						$("#contact-us-btn").attr("disabled", "disabled");
						$("#contact-us-btn").css("background", "#ccc");
						$("#contact-us-btn").css("border-bottom-color", "#aaa");

						$.ajax(
						{
							url: "../feedback.php",
							method: "POST",
							data: $("#contact-us-form").serialize()+"&tag=XRateWidget",
							success: function(data)
							{
								if ( data=="success" )
								{
									$("#contact-us-btn").val("Feedback successfully submitted.");
									$("#contact-us-btn").css("background", "#38c89c");
									$("#contact-us-btn").css("border-bottom-color", "#2DA07D");
									setTimeout(function(){
										$("#input-name").val("");
										$("#input-msg").val("");
										$("#input-email").val("");
										$("#input-phone").val("");
										Recaptcha.reload();
										$("#contact-us-btn").val("Contact Us");
										$("#contact-us-btn").removeAttr("disabled");
									}, 2000);
								}
								else
								{
									$("#contact-us-btn").val("Error Submitting Data");
									$("#contact-us-btn").css("background", "#FF9494");
									$("#contact-us-btn").css("border-bottom-color", "#FF7575");
						
									setTimeout(function(){
										Recaptcha.reload();
										$("#contact-us-btn").val("Contact Us");
										$("#contact-us-btn").removeAttr("disabled");
										$("#contact-us-btn").css("background", "#38c89c");
										$("#contact-us-btn").css("border-bottom-color", "#2DA07D");
									}, 2000);
								}
							}
						});
					}
				});
			});
			
			function isEmailValid(email)
			{
				var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return emailRegex.test(email);
			}
		</script>
	</head>
	
	<body>
	
		<table id="section-1">
			<tr>
				<td>
					<div class="title">XRateWidget</div>
					<div class="caption">
						Live Exchange Rate & Clock Widget
					</div>
					<iframe id="XRateVideo" width="560" height="315" src="//www.youtube.com/embed/i7l-wzCxmYk" frameborder="0" allowfullscreen></iframe>
					<div id="description">
						<br>XRateWidget shows the current exchange rate and displays it on a Samsung Gear device.

						<br><br>Easy switching between live exchange rate and multiple clock faces. 151 currency formats are available for currency exchange rate.

						<br><br>The market value is automatically updated in a time interval or after a manual refresh.

						<br><br>Customizable settings according to preference.
					</div>
				</td>
			</tr>
		</table>
		
		<div id="section-contact-us">
			<div id="contact-label">Leave a Message</div>

			<div id="contact-details">
				<span class="fa fa-user"></span>Anupam Basak
				<br><br>
				<span class="fa fa-map-marker"></span>Kolkata, West Bengal, India
				<br><br>
				<span class="fa fa-facebook-square"></span><a href="https://www.facebook.com/anupam.justforfun">Anupam Basak</a>
				<br><br>
				<span class="fa fa-envelope"></span><a href="mailto:teamturret@turret.in?subject=XRateWidget : " target="_blank">teamturret@turret.in</a>
				<br><br>
				<!--span class="fa fa-skype"></span>anupam.basak27-->
			</div>

			<div id="contact-form">
				<form id="contact-us-form">
					<input type="text" id="input-name" name="input-name" class="contact-form-input" placeholder="Name&emsp;(Required)">
					<br><input type="text" id="input-phone" name="input-phone" class="contact-form-input" placeholder="Phone">
					<br><input type="text" id="input-email" name="input-email" class="contact-form-input" placeholder="Email&emsp;(Required)">
					<br><textarea id="input-msg" name="input-msg" class="contact-form-input" placeholder="Message&emsp;(Required)" rows="5"></textarea>
					<br>
					<br><center>
					<script type="text/javascript" src="http://www.google.com/recaptcha/api/challenge?k=6LeNMewSAAAAAP2MpIIQNVb9ZVA06tlZy0iuPlEx"></script>

					<noscript>
				  		<iframe src="http://www.google.com/recaptcha/api/noscript?k=6LeNMewSAAAAAP2MpIIQNVb9ZVA06tlZy0iuPlEx" height="300" width="500" frameborder="0"></iframe><br/>
				  		<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
				  		<input type="hidden" name="recaptcha_response_field" value="manual_challenge"/>
					</noscript>					<center><br><input type="button" id="contact-us-btn" value="Contact Us">
				</form>
			</div>
			
			<br style="clear:both;" />
		</div>
	
	</body>
	
</html>
